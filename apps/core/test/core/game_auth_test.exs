defmodule Zen.Core.AuthTest do
  use ExUnit.Case
  alias Zen.Core.Utils.Packets.{Serializer, ClientParser}

  setup do
    opts = [:binary, packet: 0, active: false, mode: :binary]
    {:ok, socket} = :gen_tcp.connect('localhost', 11002, opts)
    %{socket: socket}
  end

  def handshake(%{socket: socket}) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    [handshake | _rest] = ClientParser.parse_all(data) |> Enum.reverse()
    assert {:handshake, data} = handshake

    :ok = :gen_tcp.send(socket, Serializer.serialize(handshake))
    {:ok, data} = :gen_tcp.recv(socket, 0)
    ClientParser.parse_all(data) |> Enum.reverse()
  end

  test "Handshake", state do
    [phase | _rest] = handshake(state)
    {:phase, phase} = phase
    assert phase == 10
  end

  describe "Login" do
    setup state do
      handshake(state)
      :ok
    end

    test "Successful Login", %{socket: socket} do
      :ok =
        :gen_tcp.send(
          socket,
          Serializer.serialize({:login, "test", "test", "", <<1>>})
        )

      {:ok, data} = :gen_tcp.recv(socket, 0)
      login_result = ClientParser.parse_all(data) |> Enum.reverse()
    end

    test "Failed Login", %{socket: socket} do
      :ok =
        :gen_tcp.send(
          socket,
          Serializer.serialize({:login, "test", "tester", "", <<1>>})
        )

      {:ok, data} = :gen_tcp.recv(socket, 0)
      [login_result | _rest] = ClientParser.parse_all(data) |> Enum.reverse()

      assert {:login_failed, "WRONGPWD\0"} = login_result
    end
  end
end
