defmodule Zen.Core.Login.Accounts do
  @accounts [{"test", "test"}]
  def authenticate(user, password) do
    case Enum.find(@accounts, fn {u, p} -> u == user and p == password end) do
      nil -> false
      {user, password} -> {user, password}
    end
  end
end
