defmodule Zen.Core.Enums do
  use Bitwise

  @damage_flag %{
    normal: 1 <<< 0,
    poison: 1 <<< 1,
    dodge: 1 <<< 2,
    block: 1 <<< 3,
    penetrate: 1 <<< 4,
    critical: 1 <<< 5,
    bleeding: 1 <<< 6
  }
  def get_damage_flag(damage_type) when is_atom(damage_type) do
    Map.get(@damage_flag, damage_type)
  end

  def get_damage_flag(damage_type) when is_binary(damage_type) or is_integer(damage_type) do
    {type, _} =
      Enum.find(@damage_flag, fn {_type, flag} ->
        damage_type == flag
      end)

    type
  end

  @char_types [
    :monster,
    :npc,
    :stone,
    :warp,
    :door,
    :building,
    :pc,
    :polymorph_pc,
    :horse,
    :goto
  ]
  def get_char_type(type) when is_atom(type) do
    @char_types
    |> Enum.find_index(fn p -> p == type end)
  end

  def get_char_type(type) when is_integer(type) do
    @char_types
    |> Enum.at(type)
  end

  @chat_types [
    :talking,
    :info,
    :notice,
    :party,
    :guild,
    :command,
    :shout,
    :whisper,
    :big_notice,
    :monarch_notice,
    :dice_info
  ]
  @spec get_chat_type(atom | integer) :: atom | integer
  def get_chat_type(type) when is_atom(type) do
    @chat_types
    |> Enum.find_index(fn p -> p == type end)
  end

  def get_chat_type(type) when is_integer(type) do
    @chat_types
    |> Enum.at(type)
  end

  @phases [
    :close,
    :handshake,
    :login,
    :select,
    :loading,
    :game,
    :dead,
    :db,
    :dbx,
    :p2p,
    :auth
  ]
  @spec get_phase(atom | integer) :: atom | integer
  def get_phase(phase) when is_atom(phase) do
    @phases
    |> Enum.find_index(fn p -> p == phase end)
  end

  def get_phase(phase) when is_integer(phase) do
    @phases
    |> Enum.at(phase)
  end

  @points [
    :none,
    :level,
    :voice,
    :exp,
    :next_exp,
    :hp,
    :max_hp,
    :sp,
    :max_sp,
    :stamina,
    :max_stamina,
    :gold,
    :st,
    :ht,
    :dx,
    :iq,
    :def_grade,
    :att_speed,
    :att_grade,
    :mov_speed,
    :client_def_grade,
    :casting_speed,
    :magic_att_grade,
    :magic_def_grade,
    :empire_point,
    :level_step,
    :stat,
    :sub_skill,
    :skill,
    :weapon_min,
    :weapon_max,
    :playtime,
    :hp_regen,
    :sp_regen,
    :bow_distance,
    :hp_recovery,
    :sp_recovery,
    :poison_pct,
    :stun_pct,
    :slow_pct,
    :critical_pct,
    :penetrate_pct,
    :curse_pct,
    :attbonus_human,
    :attbonus_animal,
    :attbonus_orc,
    :attbonus_milgyo,
    :attbonus_undead,
    :attbonus_devil,
    :attbonus_insect,
    :attbonus_fire,
    :attbonus_ice,
    :attbonus_desert,
    :attbonus_monster,
    :attbonus_warrior,
    :attbonus_assassin,
    :attbonus_sura,
    :attbonus_shaman,
    :attbonus_tree,
    :resist_warrior,
    :resist_assassin,
    :resist_sura,
    :resist_shaman,
    :steal_hp,
    :steal_sp,
    :mana_burn_pct,
    :damage_sp_recover,
    :block,
    :dodge,
    :resist_sword,
    :resist_twohand,
    :resist_dagger,
    :resist_bell,
    :resist_fan,
    :resist_bow,
    :resist_fire,
    :resist_elec,
    :resist_magic,
    :resist_wind,
    :reflect_melee,
    :reflect_curse,
    :poison_reduce,
    :kill_sp_recover,
    :exp_double_bonus,
    :gold_double_bonus,
    :item_drop_bonus,
    :potion_bonus,
    :kill_hp_recovery,
    :immune_stun,
    :immune_slow,
    :immune_fall,
    :party_attacker_bonus,
    :party_tanker_bonus,
    :att_bonus,
    :def_bonus,
    :att_grade_bonus,
    :def_grade_bonus,
    :magic_att_grade_bonus,
    :magic_def_grade_bonus,
    :resist_normal_damage,
    :hit_hp_recovery,
    :hit_sp_recovery,
    :manashield,
    :party_buffer_bonus,
    :party_skill_master_bonus,
    :hp_recover_continue,
    :sp_recover_continue,
    :steal_gold,
    :polymorph,
    :mount,
    :party_haste_bonus,
    :party_defender_bonus,
    :stat_reset_count,
    :horse_skill,
    :mall_attbonus,
    :mall_defbonus,
    :mall_expbonus,
    :mall_itembonus,
    :mall_goldbonus,
    :max_hp_pct,
    :max_sp_pct,
    :skill_damage_bonus,
    :normal_hit_damage_bonus,
    :skill_defend_bonus,
    :normal_hit_defend_bonus,
    :pc_bang_exp_bonus,
    :pc_bang_drop_bonus,
    :ramadan_candy_bonus_exp,
    :energy,
    :energy_end_time,
    :costume_attr_bonus,
    :magic_att_bonus_per,
    :melee_magic_att_bonus_per,
    :resist_ice,
    :resist_earth,
    :resist_dark,
    :resist_critical,
    :resist_penetrate,
    :bleeding_reduce,
    :bleeding_pct,
    :attbonus_wolfman,
    :resist_wolfman,
    :resist_claw,
    :accedrain_rate,
    :resist_magic_reduction
  ]

  def get_point(point) when is_integer(point) do
    @points |> Enum.at(point)
  end

  def get_point(point) when is_atom(point) do
    @points |> Enum.find_index(fn x -> x === point end)
  end

  @move_func_types [
    :wait,
    :move,
    :attack,
    :combo,
    :mob_skill
  ]
  defp is_skill?(move_func) do
    Bitwise.band(move_func, 0x80) == 1
  end

  @spec get_move(atom | integer) :: atom | integer
  def get_move(type) when is_atom(type) do
    if type == :skill do
      0x80
    else
      @move_func_types |> Enum.find_index(fn move -> move == type end)
    end
  end

  def get_move(type) when is_integer(type) do
    case is_skill?(type) do
      true ->
        :skill

      false ->
        @move_func_types |> Enum.at(type)
    end
  end
end
