defmodule Zen.Core.World.Map do
  use GenServer
  require Logger

  alias Zen.Core.World.{MonsterRegeneration, Combat, Entities}
  use Bitwise

  defp init_map(map) do
    map = Zen.Core.Prototypes.Maps.by_id(map)
    %{map| regen:
          Enum.map(
            map.regen,
            fn definition ->
              {make_ref(), %{definition: definition, mobs: []}}
            end
          )
    }
  end

  def child_spec(opts) do
    map_id = Keyword.get(opts, :map_id, __MODULE__)

    %{
      id: get_name(map_id),
      start: {__MODULE__, :start_link, [opts]},
      shutdown: 10_000,
      restart: :transient
    }
  end

  def start_link([map_id: map_id] = config) do
    case GenServer.start_link(__MODULE__, config, name: via_tuple(map_id)) do
      {:ok, pid} ->
        {:ok, pid}

      {:error, {:already_started, pid}} ->
        Logger.info("#{get_name(map_id)} Already started at #{inspect(pid)}, returning :ignore")
        :ignore
    end
  end

  @impl true
  def handle_cast({:command_target, target_id, from}, %{entities: entities} = state ) do
    target = Entities.by_id(target_id, entities)
    char = Entities.by_id(from, entities)

    if(target != nil and char != nil) do
      GenServer.cast(char.pid, {:target, target.id, target.points.hp / target.points.max_hp})
    else
      GenServer.cast(char.pid, {:target, target_id, 0})
    end

    {:noreply, state}
  end

  def handle_cast({:on_click, _target, _from},state) do
    {:noreply, state}
  end

  def handle_cast(
        {:attack, type, who, from},
        %{entities: entities} = state
      ) do
    enemy = Map.get(entities, who)
    attacker = Map.get(entities, from)

    state =
      if attacker != nil and enemy != nil  do
        Combat.attack(attacker, enemy, type, state)
      else
        state
      end

    IO.inspect("#{from} #{inspect(attacker.character.name)} ATTACKED #{who} ")
    {:noreply, state}
  end

  @impl true
  def handle_call(
        {:move, _func, _arg, rot, x, y, _time, vnum} = move,
        {_pid, _tag},
        %{map: %{base_position: [base_x, base_y]}} = state
      ) do
    reschedule_systems(state)

    map_player = get_in(state, [:entities, vnum])
    position = map_player.position
    global_player = %{map_player | position: %{position | x: x, y: y, angle: rot}, view: []}

    local_player = %{
      map_player
      | position: %{position | x: x - base_x, y: y - base_y, angle: rot}
    }

    state = put_in(state, [:entities, vnum], local_player)

    inform_players(:move, Map.delete(state.entities, vnum), move)
    {:reply, global_player, state}
  end

  def handle_call(:base_position, _from, state) do
    {:reply, state.map.base_position, state}
  end

  def handle_call(:player_count, _from, %{player_count: player_count, entities: entities} = state) do
    Enum.each(entities, fn {_id, ent} ->
      if(Entities.is_player?(ent)) do
        IO.inspect(ent)
      end
    end)

    {:reply, player_count, state}
  end

  def handle_call({:add_player, player}, {from, _tag}, state) do
    state =
      put_in(state, [:entities, player.id], %{player | pid: from})
      |> Entities.update_views()

    state = %{state | player_count: state.player_count + 1}

    Process.monitor(from)

    {:reply, self(), state}
  end

  @impl true
  def handle_info(:reschedule, %{player_count: count} = state) do
    case count do
      0 ->
        {:noreply, state, :hibernate}

      _ ->
        state =
          state
          |> schedule_actions()
          |> reschedule_systems()

        {:noreply, state}
    end
  end

  @impl true
  def handle_info({:DOWN, _ref, :process, process, _reason}, %{entities: entities} = state) do
    {_, player} =
      Enum.find(entities, fn {_, entity} ->
        Entities.is_player?(entity) and entity.pid == process
      end)

    inform_players(:delete_character, entities, player)
    entities = Map.delete(entities, player.id)
    state = %{state | player_count: state.player_count - 1, entities: entities}
    {:noreply, state}
  end

  @impl true
  def handle_continue(:init_map, %{map_id: map_id} = state) do
    map = init_map(map_id)
    {:noreply, %{state | map: map, tree: Entities.create_tree(map)}}
  end

  def schedule_actions(state) do
    state
    |> MonsterRegeneration.update()
    |> Entities.update_tree()
    |> Entities.update_views()
  end

  def reschedule_systems(%{timers: %{reschedule: timer}} = state) do
    put_in(
      state,
      [:timers, :reschedule],
      case Process.read_timer(timer) do
        false ->
          Process.send_after(self(), :reschedule, Enum.random(50..200))

        x ->
          timer
      end
    )
  end

  @impl true
  def init(map_id: map_id) do
    Process.register(self(), get_name(map_id) |> String.to_atom())
    timer = Process.send_after(self(), :reschedule, 10)

    {:ok,
     %{
       map_id: map_id,
       map: nil,
       tree: nil,
       entities: %{},
       timers: %{reschedule: timer},
       last_positions: [],
       player_count: 0
     }, {:continue, :init_map}}
  end

  def inform_about_others(:characters, %{entities: entitites}, from) do
    Enum.each(Map.values(entitites), fn %{character: {_, character}} ->
      GenServer.cast(from, {:add_character, character})
    end)
  end

  def inform_players(action, entities, message) do
    Enum.each(entities, fn {_vnum, char} ->
      if Entities.is_player?(char) do
        case action do
          :move ->
            mess = Tuple.to_list(message)
            uuid = List.last(mess)

            if Enum.any?(char.view, fn ent_uuid -> ent_uuid == uuid end) do
              GenServer.cast(char.pid, {action, message})
            end

          _ ->
            GenServer.cast(char.pid, {action, message})
        end
      end
    end)
  end

  def get_name(name) do
    Atom.to_string(__MODULE__) <> "_" <> name
  end

  def get_base_position(id) do
    GenServer.call(via_tuple(id), :base_position)
  end

  def get_player_count(id) do
    GenServer.call(via_tuple(id), :player_count)
  end

  def add_player(%Zen.Core.World.SimplePlayer{position: pos} = player) do
    {id, _map} = Zen.Core.Prototypes.Maps.by_position({pos.x, pos.y})

    GenServer.call(via_tuple(id), {:add_player, player})
  end

  def via_tuple(name) do
    {:via, Horde.Registry, {Zen.Core.GameRegistry, get_name(name)}}
  end
end
