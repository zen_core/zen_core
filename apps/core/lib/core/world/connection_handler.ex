defmodule Zen.Core.World.ConnectionHandler do
  use GenServer
  require Logger
  alias alias Zen.Core.Utils.Packets
  alias Zen.Core.Utils.Position
  alias Zen.Core.Utils.Packets.{Serializer, Parser}
  use Bitwise
  @behaviour :ranch_protocol

  @impl true
  def start_link(ref, socket, transport) do
    Logger.info("Starting World Player Connection Handler")
    {:ok, :proc_lib.spawn_link(__MODULE__, :init, [{ref, socket, transport}])}
  end

  @impl true
  def init({ref, transport, _opts}) do
    {:ok, socket} = :ranch.handshake(ref)

    # , {:header, 1}
    :ok = transport.setopts(socket, [{:active, true}, {:packet, 0}, {:mode, :binary}])
    Process.flag(:trap_exit, true)
    transport.send(socket, Serializer.serialize({:phase, :login}))
    # handshake()
    ping()

    :gen_server.enter_loop(
      __MODULE__,
      [],
      %{
        ref: ref,
        transport: transport,
        login_state: :new,
        socket: socket,
        characters: nil,
        character: nil,
        last_pong: nil,
        last_ping: nil,
        target: nil
      }
    )
  end

  defp get_channel_status(%{ref: ref}) do
    :ranch.get_status(ref)
  end

  defp send_channel_status(:running, %{socket: socket, transport: transport, ref: ref} = state) do
    %{
      socket_opts: [
        port: port
      ]
    } = :ranch.get_transport_options(ref)

    data = Serializer.serialize({:channel_status, [{port, 1}]})

    transport.send(socket, data)
    state
  end

  defp ping() do
    Process.send_after(self(), :ping, 4000)
  end

  @impl true
  def handle_cast({:add_character, character}, state) do
    new_state =
      Packets.send_packet(state, {
        :send_character,
        character
      })

    case character.character.type do
      :pc ->
        Packets.send_packets(state, [
          {
            :send_character_additional,
            character
          },
          {
            :walk_mode,
            {character.id, 0}
          }
        ])

      _other ->
        state
    end

    {:noreply, new_state}
  end

  def handle_cast({:damage, who, type, amount}, state) do
    Packets.send_packet(state, {:damage, {who, type, amount}})

    {:noreply, state}
  end

  def handle_cast({:target, who, hp}, state) do
    Packets.send_packet(state, {:target, {who, hp}})

    {:noreply, state}
  end

  @impl true
  def handle_cast({:chat, {type, vid, empire, message}}, state) do
    state =
      state
      |> Packets.send_packet({
        :chat,
        type,
        vid,
        empire,
        message
      })

    {:noreply, state}
  end

  def handle_cast({:delete_character, character}, state) do
    Packets.send_packet(state, {
      :delete_character,
      character
    })

    {:noreply, state}
  end

  def handle_cast({:move, move}, state) do
    Packets.send_packet(state, {
      :move,
      move
    })

    {:noreply, state}
  end

  defp register_process(character) do
    name = ("player_" <> character.character.name) |> String.to_atom()

    if name in Process.registered() do
    else
      Process.register(self(), name)
    end
  end

  defp chat_message(state, type, vid, empire, message) do
    Packets.send_packet(state, {
      :chat,
      type,
      vid,
      empire,
      message
    })
  end

  defp send_notice(state, message),
    do: chat_message(state, :notice, 0, 0, message)

  defp send_command(state, message),
    do: chat_message(state, :command, 0, 0, message)

  @impl true
  def handle_info(
        {:tcp, _socket, data},
        state
      ) do
    packets =
      Parser.parse_all(data)
      |> Enum.reverse()

    state =
      packets
      |> Enum.reduce(state, &handle/2)

    {:noreply, state}
  end

  def handle_info(
        :handshake,
        state
      ) do
    Logger.info("HANDSHAKE WORLD")
    key = :rand.uniform(32000)

    now =
      DateTime.utc_now()
      |> DateTime.to_unix()

    Packets.send_packet(
      state,
      {:handshake, {key, now, 0}}
    )

    {:noreply, Map.put(state, :login_state, :handshake)}
  end

  def handle_info(
        :ping,
        state
      ) do
    Packets.send_packet(state, {:ping})
    ping()
    {:noreply, Map.put(state, :last_ping, DateTime.utc_now())}
  end

  def handle_info({:tcp_closed, socket}, state = %{socket: socket, transport: transport}) do
    transport.close(socket)

    state = Map.drop(state, [:socket, :transport])

    {:stop, :normal, state}
  end

  def handle_info(:retry_map, state) do
    state = handle({:enter_game}, state)
    {:noreply, state}
  end

  def handle_info({:DOWN, _ref, :process, process, reason}, %{map_server: map_server} = state)
      when process == map_server do
    Logger.error("Players Monitored Process died #{inspect(process)} for reason #{reason}")
    Process.send_after(self(), :retry_map, 1000)
    {:noreply, state}
  end

  def handle_info({:DOWN, _ref, :process, process, reason}, state) do
    Logger.error("Process died #{inspect(process)} for reason #{reason}")
    {:noreply, state}
  end

  def handle_info(unk1, state) do
    Logger.error("UNKNOWN PACKET: #{inspect(unk1)}")
    {:noreply, state}
  end

  defp handle(
         {:enter_game},
         state = %{
           character: character
         }
       ) do
    register_process(character)

    now =
      DateTime.utc_now()
      |> DateTime.to_unix()

    Packets.send_packets(state, [
      {:set_time, now},
      {:set_channel, 1},
      {:send_character, character},
      {:send_character_additional, character},
      {:send_points, character}
    ])

    send_notice(state, "WELCOME TO MY SERVER")
    send_command(state, "ConsoleEnable")
    # @TODO: INVENTORY

    pid = Zen.Core.World.Map.add_player(character)
    Process.monitor(pid)

    state
    |> Map.put(:map_server, pid)
    |> Map.put(:login_state, :game)
  end

  defp handle(
         {:chat, _type, "/phase_select"},
         state
       ) do
    Packets.send_packet(state, {:phase, :select})
    Map.put(state, :login_state, :select)
    state
  end

  defp handle(
         {:chat, _type, message},
         state
       ) do
    IO.inspect(message)
    state
  end

  defp handle(
         {:mark_index_request},
         state
       ) do
    Logger.info("Handle Handshake")
    Packets.send_packet(state, {:mark_index, 1, [{1, 1}]})
    Map.put(state, :login_state, :auth)
  end

  defp handle(
         {:command_target, target} = command,
         state = %{map_server: map_server, character: char}
       ) do
    GenServer.cast(map_server, Tuple.append(command, char.id))
    %{state | target: target}
  end

  defp handle(
         {:on_click, target} = click,
         state = %{map_server: map_server, character: char}
       ) do
    GenServer.cast(map_server, Tuple.append(click, char.id))
    state
  end

  defp handle(
         {:command_character_select, player_index},
         state = %{
           characters: characters
         }
       ) do
    character = Enum.at(characters, player_index)
    Logger.info("Player Selected Character #{character.character.name}")
    Packets.send_packets(state, [{:phase, :loading}, {:main_character, character}])

    state
    |> Map.put(:login_state, :loading)
    |> Map.put(:character, character)
    |> Map.delete(:characters)
  end

  defp handle({:mark_crclist, _img_index, _crc_list}, state) do
    Logger.warning("MARK CRCLIST NOT IMPLEMENTED")
    state
  end

  defp handle(
         {:client_version, {_filename, _timestamp}},
         state
       ) do
    # Logger.info("Handle Client Version #{filename}:#{timestamp}")
    Packets.send_packet(state, {:phase, :game})
    state
  end

  defp handle(
         {:handshake, {_handshake, _time, _delta}},
         state
       ) do
    Logger.info("Handle Handshake")
    Packets.send_packet(state, {:phase, :handshake})
    Map.put(state, :login_state, :auth)
  end

  defp handle({:pong}, state), do: %{state | last_pong: DateTime.utc_now()}

  defp handle(
         {:move, _func, _arg, _rot, _x, _y, _time} = move,
         state = %{map_server: map_server, character: char}
       ) do
    character = GenServer.call(map_server, Tuple.append(move, char.id))
    %{state | character: character}
  end

  defp handle(
         {:request_channel_status},
         state
       ) do
    Logger.info("request_channel_status")

    get_channel_status(state)
    |> send_channel_status(state)
  end

  defp make_player(vnum, armor, weapon) do
    base_player = %{
      %Zen.Core.World.SimplePlayer{}
      | id: :rand.uniform(4_294_967_290),
        position: %Position{x: 423_364, y: 931_464},
        points: %{
          level: 50,
          voice: 0,
          exp: 0,
          next_exp: 10,
          hp: 1000,
          max_hp: 10000,
          sp: 50,
          max_sp: 100,
          stamina: 20,
          max_stamina: 20,
          gold: 1000,
          st: 10,
          ht: 10,
          dx: 10,
          iq: 10,
          def_grade: 0,
          att_speed: 200,
          att_grade: 0,
          mov_speed: 900
        }
    }

    %{
      base_player
      | character: %{base_player.character | vnum: vnum, armor: armor, weapon: weapon}
    }
  end

  defp handle(
         {:login2, {_login, _login_key, _key, _sequence}},
         state
       ) do
    # @TODO: Check Login Keys

    Packets.send_packet(state, {:empire, 3})
    Packets.send_packet(state, {:phase, :select})

    # @TODO: Grab Characters from Account Server
    characters = [
      make_player(0, 11209, 119),
      make_player(2, 11219, 119),
      make_player(4, 11229, 119),
      make_player(6, 11239, 119)
    ]

    Packets.send_packet(state, {
      :login_success_data,
      characters
    })

    %{state | characters: characters}
  end

  defp handle(
         {:empire_select, {empire, _seq}},
         state
       ) do
    Logger.info("Player Selected Empire: #{empire}")
    Packets.send_packet(state, {:phase, :select})
    state
  end

  defp handle(
         {:attack, {type, who}},
         state = %{map_server: map_server, character: char}
       ) do
    GenServer.cast(map_server, {:attack, type, who, char.id})
    state
  end
end
