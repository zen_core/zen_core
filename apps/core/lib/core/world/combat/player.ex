defmodule Zen.Core.World.Combat.Player do
  alias Zen.Core.World.{Combat, Entities}

  alias Zen.Core.Prototypes.DataBank

  def get_defence(character) do
    enemy_armor = DataBank.get_item(character.character.armor)
    enemy_armor.value1 + enemy_armor.value5 * 2
  end

  def calculate_damage(attacker, enemy) do
    weapon = DataBank.get_item(attacker.character.weapon)
    dmg = Enum.random(weapon.value3..weapon.value4)

    defence = Combat.get_defence(enemy)

    dmg = dmg - defence
    dmg = if dmg < 0, do: 0, else: dmg
    {:normal, dmg}
  end

  def is_alive?(who) do
    who.points.hp > 0
  end

  def apply_damage({_type, amount}, enemy, %{entities: entities} = state) do
    case Combat.is_alive?(enemy) do
      true ->
        new_hp = enemy.points.hp - amount

        if new_hp <= 0 do
          %{state | entities: Map.delete(entities, enemy.id)}
        else
          put_in(state, [:entities, enemy.id, :points], %{enemy.points | hp: new_hp})
        end

      false ->
        state
    end
  end
end
