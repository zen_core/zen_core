defmodule Zen.Core.World.Combat.Monster do
  alias Zen.Core.World.{Combat, Entities}
  alias Zen.Core.Prototypes.DataBank

  def get_defence(character) do
    character.character.def
  end

  def calculate_damage(attacker, enemy) do
    mob = DataBank.get_mob(attacker.character.vnum)
    {:normal, Enum.random(mob.min_damage..mob.max_damage)}
  end

  def is_alive?(who) do
    who.points.hp > 0
  end

  def apply_damage({_type, amount}, enemy, %{entities: entities} = state) do
    case Combat.is_alive?(enemy) do
      true ->
        new_hp = enemy.points.hp - amount

        if new_hp <= 0 do
          %{state | entities: Map.delete(entities, enemy.id)}
        else
          put_in(state, [:entities, enemy.id, :points], %{enemy.points | hp: new_hp})
        end

      false ->
        state
    end
  end
end
