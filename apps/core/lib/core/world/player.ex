defmodule Zen.Core.World.Stats do
  defstruct strength: 50, health: 50, dexterity: 50, intelligence: 50
end

defmodule Zen.Core.World.ServerAddress do
  defstruct ip: "127.0.0.1", port: 13001
end

defmodule Zen.Core.World.SimplePlayer do
  alias Zen.Core.Utils.Position
  alias Zen.Core.World.{Stats, ServerAddress}
  use Bitwise
  defdelegate fetch(term, key), to: Map
  defdelegate get(term, key, default), to: Map
  defdelegate get_and_update(term, key, fun), to: Map
  defdelegate pop(term, key), to: Map

  defstruct id: 1,
            pid: 0,
            position: %Position{},
            server_address: %ServerAddress{},
            view: [],
            points: %{},
            character: %{
              name: "Tester",
              vnum: 0,
              level: 50,
              minutes: 13600,
              stats: %Stats{},
              hair: 0,
              type: :pc,
              armor: 11200,
              weapon: 119,
              head: 0,
              mount: 0,
              state:
                1 <<<
                  1,
              skill_group: 0,
              empire: 0
            }

  @type t :: %__MODULE__{
          id: non_neg_integer,
          points: %{},
          character: %{
            name: String.t(),
            vnum: non_neg_integer,
            minutes: non_neg_integer,
            stats: %Stats{},
            hair: non_neg_integer,
            armor: non_neg_integer,
            skill_group: non_neg_integer,
            empire: non_neg_integer
          },
          position: %Position{},
          server_address: %ServerAddress{}
        }
end
