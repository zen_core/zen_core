defmodule Zen.Core.Prototypes.Maps do
  use Agent
  alias Zen.Core.Utils

  def start_link(_x \\ "") do
    Agent.start_link(fn -> import_data() end, name: __MODULE__)
  end

  def get_all() do
    Agent.get(
      __MODULE__,
      fn state ->
        state
      end
    )
  end

  def by_position([x, y]), do: by_position({x, y})

  def by_position({x, y}) do
    Agent.get(
      __MODULE__,
      fn state ->
        Enum.find(state, fn {_id, map} ->
          [map_x, map_y] = map.base_position
          [width, height] = map.map_size
          x_max = map_x + map.cell_scale * 128 * width
          y_max = map_y + map.cell_scale * 128 * height
          Utils.point_in_square(map_x, map_y, x_max, y_max, x, y)
        end)
      end
    )
  end

  def by_id(id) do
    Agent.get(
      __MODULE__,
      fn state ->
        Map.get(state, id)
      end
    )
  end

  defp parse_option(option, value) when option in ["BasePosition", "MapSize"] do
    value
    |> String.trim()
    |> String.replace(~r/\s+/, " ")
    |> String.split(~r/\s/)
    |> Enum.map(&String.trim/1)
    |> Enum.map(&String.to_integer/1)
  end

  defp parse_option(option, value) when option in ["CellScale", "ViewRadius"],
    do: String.to_integer(value)

  defp parse_option(option, value) when option in ["HeightScale"], do: String.to_float(value)
  defp parse_option(_option, value), do: value

  defp add_attr_size(map, name) do
    attr_path = "data/locale/singapore/map/#{name}/server_attr"
    file = File.open!(attr_path)
    data = IO.binread(file, 8)

    <<width::little-integer-size(32), height::little-integer-size(32)>> = data
    File.close(file)

    map
    |> Map.put(:attr_height, height)
    |> Map.put(:attr_width, width)
  end

  defp parse_regen_type("m"), do: :mob
  defp parse_regen_type("ma"), do: :mob_aggressive
  defp parse_regen_type("g"), do: :group
  defp parse_regen_type("ga"), do: :group_aggressive
  defp parse_regen_type("e"), do: :exception
  defp parse_regen_type("r"), do: :group_group
  defp parse_regen_type("s"), do: :anywhere

  defp add_regen(map, name) do
    regen_path = "data/locale/singapore/map/#{name}/regen.txt"

    if File.exists?(regen_path) do
      Map.put(
        map,
        :regen,
        File.stream!(regen_path, [], :line)
        |> Stream.filter(fn line ->
          trimmed = String.trim(line)
          not (trimmed == "") and not String.starts_with?(trimmed, "//")
        end)
        |> Stream.map(fn line ->
          line = String.replace_trailing(line, "\n", "")

          [type, x, y, max_x, max_y, z_section, direction, regen, percent, max_count, mob] =
            String.split(line, ~r/\s/, trim: true, parts: 11)

          {
            parse_regen_type(type),
            String.to_integer(x),
            String.to_integer(y),
            String.to_integer(max_x),
            String.to_integer(max_y),
            String.to_integer(z_section),
            String.to_integer(direction),
            regen,
            String.to_integer(percent),
            String.to_integer(max_count),
            String.to_integer(mob)
          }
        end)
        |> Enum.to_list()
      )
    else
      Map.put(
        map,
        :regen,
        []
      )
    end
  end

  defp load_map(name) do
    settings_path = "data/locale/singapore/map/#{name}/Setting.txt"

    File.stream!(settings_path, [], :line)
    |> Stream.filter(fn line -> not (String.trim(line) == "") end)
    |> Stream.map(fn line ->
      [key, value] = String.split(line, ~r/\s/, trim: true, parts: 2)
      key = :binary.copy(key)
      value = :binary.copy(value)
      value = String.replace_trailing(value, "\n", "")
      {String.to_atom(key |> Macro.underscore()), parse_option(key, value)}
    end)
    |> Enum.to_list()
    |> Enum.into(%{})
    |> add_attr_size(name)
    |> add_regen(name)
  end

  def import_data do
    path = "data/locale/singapore/map/index"

    File.stream!(path, [], :line)
    |> Stream.map(fn line ->
      [map_id, map_name] = String.split(line)
      map_id = :binary.copy(map_id)
      map_name = :binary.copy(map_name)
      {map_id, load_map(map_name)}
    end)
    |> Enum.to_list()
    |> Enum.into(%{})
  end
end
