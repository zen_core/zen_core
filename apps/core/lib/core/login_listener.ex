defmodule Zen.Core.LoginListener do
  use GenServer
  require Logger

  def child_spec(opts) do
    name = Keyword.get(opts, :name, __MODULE__)

    %{
      id: get_name(name),
      start: {__MODULE__, :start_link, [opts]},
      shutdown: 10_000,
      restart: :transient
    }
  end

  @spec start_link([{:name, atom} | {:port, any}, ...]) :: :ignore | {:ok, pid}
  def start_link([name: name, port: _port] = config) do
    case GenServer.start_link(__MODULE__, config, name: via_tuple(name)) do
      {:ok, pid} ->
        {:ok, pid}

      {:error, {:already_started, pid}} ->
        Logger.info("#{get_name(name)} Already started at #{inspect(pid)}, returning :ignore")
        :ignore
    end
  end

  @impl true
  @spec init([{:name, atom} | {:port, any}, ...]) ::
          :ignore
          | {:ok, pid()}
          | {:stop, pid()}
          | {:ok, pid(), :hibernate | :infinity | non_neg_integer() | {:continue, pid()}}

  def init(name: name, port: port) do
    opts = [port: port]

    Logger.info("#{get_name(name)} Listening for connections on port #{port}")

    :ranch.start_listener(
      get_name(name),
      :ranch_tcp,
      opts,
      Zen.Core.Login.ConnectionHandler,
      []
    )
  end

  @spec get_name(atom) :: atom
  def get_name(name) do
    String.to_atom(Atom.to_string(name) <> "_" <> Atom.to_string(node()))
  end

  @spec via_tuple(atom) :: {:via, Horde.Registry, {Zen.Core.GameRegistry, atom}}
  def via_tuple(name) do
    {:via, Horde.Registry, {Zen.Core.GameRegistry, get_name(name)}}
  end
end
