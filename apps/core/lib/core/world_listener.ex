defmodule Zen.Core.WorldListener do
  use GenServer
  require Logger

  def child_spec(opts) do
    name = Keyword.get(opts, :name, __MODULE__)

    %{
      id: get_name(name),
      start: {__MODULE__, :start_link, [opts]},
      shutdown: 10_000,
      restart: :transient
    }
  end

  def start_link([name: name, port: _port] = config) do
    case GenServer.start_link(__MODULE__, config, name: via_tuple(name)) do
      {:ok, pid} ->
        {:ok, pid}

      {:error, {:already_started, pid}} ->
        Logger.info("#{get_name(name)} Already started at #{inspect(pid)}, returning :ignore")
        :ignore

      x ->
        Logger.error("#{inspect(x)}")
        :ignore
    end
  end

  @impl true
  def init([name: name, port: port] = _args) do
    opts = [port: port]

    case :ranch.start_listener(
           get_name(name),
           :ranch_tcp,
           opts,
           Zen.Core.World.ConnectionHandler,
           []
         ) do
      {:ok, pid} ->
        Logger.info("#{get_name(name)} Listening for connections on port #{port}")
        {:ok, pid}

      x ->
        Logger.error("#{inspect(x)}")
    end
  end

  def get_name(name) do
    String.to_atom(Atom.to_string(name) <> "_" <> Atom.to_string(node()))
  end

  def via_tuple(name) do
    {:via, Horde.Registry, {Zen.Core.GameRegistry, get_name(name)}}
  end
end
