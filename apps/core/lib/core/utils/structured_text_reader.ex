defmodule Strip do
  @doc """
    iex> Strip.strip_utf "Tallak\xc3\xb1 Tveide"
    "Tallakñ Tveide"
  """
  def strip_utf(str) do
    strip_utf_helper(str, [])
  end

  defp strip_utf_helper(<<x::utf8>> <> rest, acc) do
    strip_utf_helper(rest, [x | acc])
  end

  defp strip_utf_helper(<<x>> <> rest, acc), do: strip_utf_helper(rest, acc)

  defp strip_utf_helper("", acc) do
    acc
    |> :lists.reverse()
    |> List.to_string()
  end
end

defmodule Zen.Core.Utils.StructuredTextReader do
  import NimbleParsec

  whitespace_newline = utf8_string([?\s, ?\t, ?\n, ?\r], min: 1)
  newline = utf8_string([?\n, ?\r], min: 1)

  whitespace = utf8_string([?\s, ?\t], min: 1)
  value_name = utf8_string([?a..?z, ?A..?Z, ?_], min: 1)
  string_value = value_name
  integer_value = integer(min: 1) |> lookahead_not(string("."))

  defparsec(
    :weird_string,
    utf8_string([{:not, ?\r}, {:not, ?\n}, {:not, ?\s}, {:not, ?\t}], min: 1)
  )

  value =
    choice([
      string_value,
      integer_value,
      ignore(whitespace),
      utf8_string([{:not, ?\r}, {:not, ?\n}, {:not, ?\t}], min: 1)
    ])

  group_name =
    choice([string("Group"), string("List")])
    |> replace(:group)
    |> repeat(ignore(whitespace))
    |> repeat(
      lookahead_not(newline)
      |> utf8_string([{:not, ?\r}, {:not, ?\n}, {:not, ?\t}], min: 1)
      |> optional(ascii_char([?\_]))
    )
    |> repeat(ignore(whitespace_newline))
    |> ignore(utf8_char([?{]))

  group_end = repeat(ignore(whitespace_newline)) |> utf8_char([?}]) |> replace(:group_end)

  comment =
    ignore(repeat(whitespace))
    |> utf8_char([?#])
    |> eventually(utf8_char([?\n, ?\r]))

  data_line =
    repeat(value)
    |> eventually(ignore(whitespace_newline))
    |> wrap()

  text_file_loader =
    repeat(
      choice([ignore(comment), group_name, ignore(whitespace_newline), group_end, data_line])
    )

  defparsec(:data_line, data_line)
  defparsec(:value_name, value_name)
  defparsec(:text_file_loader, text_file_loader)

  @doc """
  Parses the Given `file`
  ## Examples
      iex> Zen.Core.Utils.StructuredTextReader.parse_text( "Group Test
      ...>{
      ...>      Group Default
      ...>      {
      ...>          #--#    1    2    3    4    5
      ...>      # ��� ������(%)
      ...>          CHARGING    0    5    15    30    50
      ...>      #    ��ް� �������� ���� Ȯ�� ���̺�
      ...>      #    ���     Ȯ��
      ...>      }
      ...>  }")
      {:ok, %{"ROOT"=> %{"Test"=>%{"Default"=>%{props: [["CHARGING",0,5,15,30,50]]}}}}}
  """
  def parse_text(text) do
    with {:ok, parsed, _, _, _, _} <-
           text_file_loader(text) do
      parsed = [:group, "ROOT"] ++ parsed ++ [:group_end]
      parsed = parsed |> Enum.filter(fn x -> not (is_list(x) and x == []) end)

      construct_group(parsed, %{}, [])
    end
  end

  def parse_file(path) do
    file = File.open!(path, [:raw])
    file = IO.binread(file, :all)
    file = Strip.strip_utf(file)

    with {:ok, parsed, _, _, _, _} <-
           text_file_loader(file) do
      parsed = [:group, "ROOT"] ++ parsed ++ [:group_end]
      parsed = parsed |> Enum.filter(fn x -> not (is_list(x) and x == []) end)

      construct_group(parsed, %{}, []) |> IO.inspect(limit: :infinity)
    end
  end

  defp construct_group([:group, name | rest], acc, path) do
    new_path = path ++ [name]

    construct_group(rest, put_in(acc, new_path, %{}), new_path)
  end

  defp construct_group([:group_end | rest], acc, path) do
    new_path = :lists.reverse(path) |> tl() |> :lists.reverse()
    construct_group(rest, acc, new_path)
  end

  defp construct_group([], acc, []) do
    {:ok, acc}
  end

  defp construct_group([head | rest] = groups, acc, path)
       when is_list(groups) and is_list(head) and length(head) > 0 do
    construct_group(head, rest, acc, path)
  end

  defp construct_group(other, acc, path) do
    {:error, {other, acc, path}}
  end

  defp construct_group(props, rest, acc, path) do
    list = get_in(acc, path)

    new_list =
      cond do
        is_map(list) ->
          if Map.has_key?(list, :props) do
            Map.put(list, :props, list.props ++ [props])
          else
            Map.put_new(list, :props, [props])
          end
      end

    {_, new} = get_and_update_in(acc, path, fn x -> {x, new_list} end)
    construct_group(rest, new, path)
  end
end
