defmodule Zen.Core.Utils.Position do
  use Bitwise
  defdelegate fetch(term, key), to: Map
  defdelegate get(term, key, default), to: Map
  defdelegate get_and_update(term, key, fun), to: Map
  defstruct x: 0, y: 0, z: 0, angle: 0

  def create(x: x, y: y, z: z, angle: angle) do
    %__MODULE__{x: x, y: y, z: z, angle: angle}
  end
end
