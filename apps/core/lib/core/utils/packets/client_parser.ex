defmodule Zen.Core.Utils.Packets.ClientParser do
  @moduledoc """
    AS A CLIENT PARSE SERVER PACKETS
    for Simulator/LoadTesting/Tests
  """
  require Logger
  @type packet_data :: tuple()
  @type unprocessed_data :: binary() | <<>>
  @type parsed_packet :: {packet_data(), unprocessed_data()}

  @spec parse_all({binary()}) :: [parsed_packet()]

  def parse_all(<<header, data::binary()>>, acc \\ []) do
    {packet, rest} = parse(header, data)

    case rest do
      "" ->
        [packet | acc]

      x ->
        parse_all(x, [packet | acc])
    end
  end

  @spec parse(non_neg_integer(), binary()) :: parsed_packet

  def parse(150, <<key::little-integer-size(32), result::binary-size(1), rest::binary()>>) do
    {{:login_success, key, result}, rest}
  end

  def parse(7, <<status::binary-size(9), rest::binary>>) do
    {{:login_failed, status}, rest}
  end

  def parse(253, <<phase::little-integer-size(8), rest::binary()>>) do
    {{:phase, phase}, rest}
  end

  def parse(255, <<
        handshake::little-integer-size(32),
        time::little-integer-size(32),
        delta::little-integer-size(32),
        rest::binary
      >>) do
    {{:handshake, {handshake, time, delta}}, rest}
  end

  # def parse(111,_x) do
  # {:login3, {"login","passwd","key"}}
  #  |>IO.inspect
  # end
end
