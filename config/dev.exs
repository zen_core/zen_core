import Config

config :logger, :console,
  level: :info,
  format: "[$level] $message $metadata\n",
  metadata: [:error_code, :file]
